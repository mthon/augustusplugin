package com.michaelrthon.augustusPlugin;

import com.biomatters.geneious.publicapi.documents.AnnotatedPluginDocument;
import com.biomatters.geneious.publicapi.documents.sequence.NucleotideSequenceDocument;
import com.biomatters.geneious.publicapi.documents.sequence.SequenceDocument;
import com.biomatters.geneious.publicapi.plugin.DocumentOperationException;
import com.biomatters.geneious.publicapi.plugin.DocumentSelectionSignature;
import com.biomatters.geneious.publicapi.plugin.GeneiousActionOptions;
import com.biomatters.geneious.publicapi.plugin.Options;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator;
import com.biomatters.geneious.publicapi.utilities.Execution;
import com.biomatters.geneious.publicapi.utilities.FileUtilities;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jebl.util.ProgressListener;

public class AugustusAnnotationGenerator extends SequenceAnnotationGenerator {

    static final String HELP = "Augustus finds genes in eukaryotic genome sequences";

    @Override
    public GeneiousActionOptions getActionOptions() {
        // TODO Auto-generated method stub
        return new GeneiousActionOptions("Find Genes with Augustus...")
                .setMainMenuLocation(GeneiousActionOptions.MainMenu.AnnotateAndPredict);
    }

    @Override
    public String getHelp() {
        return HELP;
    }

    @Override
    public DocumentSelectionSignature[] getSelectionSignatures() {
        return new DocumentSelectionSignature[]{
            new DocumentSelectionSignature(NucleotideSequenceDocument.class, 1, Integer.MAX_VALUE)
        };
    }

    public Options getOptions(AnnotatedPluginDocument[] documents,
            SequenceAnnotationGenerator.SelectionRange selectionRange)
            throws DocumentOperationException {
        return new AugustusOptions();
    }
    
    @Override
    public Options getGeneralOptions() {
        return new AugustusOptions();
    }

	
	@Override
    public List<SequenceAnnotationGenerator.AnnotationGeneratorResult> generate(
            AnnotatedPluginDocument[] documents,
            SequenceAnnotationGenerator.SelectionRange selectionRange,
            ProgressListener progressListener, Options options)
            throws DocumentOperationException {

        List<SequenceAnnotationGenerator.AnnotationGeneratorResult> resultsList = new ArrayList<SequenceAnnotationGenerator.AnnotationGeneratorResult>();

        AugustusOptions augustusOptions = (AugustusOptions) options;

        File tempSeq = null;

        for (AnnotatedPluginDocument doc : documents) {
            SequenceDocument seqDoc = (SequenceDocument) doc.getDocument();
            tempSeq = writeFasta(tempSeq, seqDoc);
            String[] command = augustusOptions.getCommand(tempSeq
                    .getAbsolutePath());

            try {
                AugustusExecutionOutputListener outputListener = new AugustusExecutionOutputListener();

                Execution exec = new Execution(command, progressListener, outputListener, (String) null, false);
                String workDir = augustusOptions.getWorkingDir();
                exec.setWorkingDirectory(workDir );
                
                exec.execute();
                if (exec.wasKilledByGeneious()) {
                    return null;
                }
                resultsList.add(outputListener.getResults());

            } catch (IOException e) {
                throw new DocumentOperationException("Augustus failed:" + e.getMessage(), e);
            } catch (InterruptedException e) {
                throw new DocumentOperationException("Process Killed", e);
            }

        }

        return resultsList;
    }

    private File writeFasta(File tempFile, SequenceDocument seqDoc)
            throws DocumentOperationException {
        // Create temporary FASTA file for Phobos to read.
        try {
            tempFile = FileUtilities.createTempFile("temp_augustus", ".fasta",
                    true);
            // Delete temp file when program exits.
            tempFile.deleteOnExit();
            // write to FASTA format
            BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));
            out.write(">" + seqDoc.getName() + " " + seqDoc.getDescription()
                    + "\n");
            out.write(seqDoc.getSequenceString().toUpperCase());
            out.close();
        } catch (IOException e) {
            throw new DocumentOperationException("Failed to write FASTA file:"
                    + e.getMessage(), e);
        }
        return tempFile;
    }
}
