package com.michaelrthon.augustusPlugin;

import com.biomatters.geneious.publicapi.documents.sequence.SequenceAnnotation;
import com.biomatters.geneious.publicapi.documents.sequence.SequenceAnnotationInterval;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator;
import com.biomatters.geneious.publicapi.utilities.Execution;

public class AugustusExecutionOutputListener extends Execution.OutputListener {

    final SequenceAnnotationGenerator.AnnotationGeneratorResult results =
            new SequenceAnnotationGenerator.AnnotationGeneratorResult();
    private SequenceAnnotation cdsAnnotation = null;

    SequenceAnnotationGenerator.AnnotationGeneratorResult getResults() {
        return results;
    }

    @Override
    public void stderrWritten(String arg0) {
        //System.out.println("stderr " + arg0);
    }

    @Override
    public void stdoutWritten(String arg0) {

        //System.out.println("stdout " + arg0);
        String columns[] = arg0.split("\\t");

        if (columns.length == 9 && columns[2].equals("gene")) {


            SequenceAnnotationInterval.Direction direction;

            if (columns[6].equals("-")) {
                direction = SequenceAnnotationInterval.Direction.rightToLeft;
            } else {
                direction = SequenceAnnotationInterval.Direction.leftToRight;
            }

            int startPos = Integer.parseInt(columns[3].trim());
            int stopPos = Integer.parseInt(columns[4].trim());

            final SequenceAnnotationInterval interval =
                    new SequenceAnnotationInterval(startPos, stopPos, direction);
            SequenceAnnotation annotation = new SequenceAnnotation(columns[8],
                    SequenceAnnotation.TYPE_GENE, interval);
            results.addAnnotationToAdd(annotation);
        }

        if (columns.length == 9 && columns[2].equals("transcript")) {
            //first initialize the CDS annotation
            cdsAnnotation = new SequenceAnnotation(columns[8], SequenceAnnotation.TYPE_CDS);
            
            SequenceAnnotationInterval.Direction direction;

            if (columns[6].equals("-")) {
                direction = SequenceAnnotationInterval.Direction.rightToLeft;
            } else {
                direction = SequenceAnnotationInterval.Direction.leftToRight;
            }

            int startPos = Integer.parseInt(columns[3].trim());
            int stopPos = Integer.parseInt(columns[4].trim());

            final SequenceAnnotationInterval interval =
                    new SequenceAnnotationInterval(startPos, stopPos, direction);
            SequenceAnnotation annotation = new SequenceAnnotation(columns[8],
                    SequenceAnnotation.TYPE_MRNA, interval);
            results.addAnnotationToAdd(annotation);
        }

        if (columns.length == 9 && columns[2].equals("CDS")) {

            SequenceAnnotationInterval.Direction direction;

            if (columns[6].equals("-")) {
                direction = SequenceAnnotationInterval.Direction.rightToLeft;
            } else {
                direction = SequenceAnnotationInterval.Direction.leftToRight;
            }

            int startPos = Integer.parseInt(columns[3].trim());
            int stopPos = Integer.parseInt(columns[4].trim());

            final SequenceAnnotationInterval interval =
                    new SequenceAnnotationInterval(startPos, stopPos, direction);
            
            cdsAnnotation.addInterval(interval);
        }
        
        if (columns[0].contains("end gene")) {
            results.addAnnotationToAdd(cdsAnnotation);
            cdsAnnotation = null;
        }
    }
}
