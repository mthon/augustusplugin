Authors
=======
Michael Thon mike@michaelrthon.com

Description
===========
A Geneious plugin for running Augustus, a eukaryotic gene prediction program.

Installation
============
For Geneious users, you can install the plugin from the plugin manager. Start
Geneious and go to the menu Tools > Plugins.

Development Notes
=================
the development notes are under development.