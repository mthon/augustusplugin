#!/usr/bin/python

from os import listdir

species_path = 'augustus.2.7/config/species'

for s in listdir(species_path):
    print 'speciesList.add(new OptionValue("%s","%s"));' % (s,s)