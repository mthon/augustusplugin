package com.michaelrthon.augustusPlugin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import com.biomatters.geneious.publicapi.plugin.Options;
import com.biomatters.geneious.publicapi.plugin.Options.FileSelectionOption;
import java.io.File;

public class AugustusOptions extends Options {

    private ComboBoxOption speciesOption;

    public AugustusOptions() {

        OptionValue defaultSpecies = new OptionValue("aedes", "Aedes");

        ArrayList<OptionValue> speciesList = new ArrayList<OptionValue>();

        speciesList.add(defaultSpecies);

        speciesList.add(new OptionValue("amphimedon", "amphimedon"));
        speciesList.add(new OptionValue("anidulans", "anidulans"));
        speciesList.add(new OptionValue("arabidopsis", "arabidopsis"));
        speciesList.add(new OptionValue("aspergillus_fumigatus", "aspergillus_fumigatus"));
        speciesList.add(new OptionValue("aspergillus_nidulans", "aspergillus_nidulans"));
        speciesList.add(new OptionValue("aspergillus_oryzae", "aspergillus_oryzae"));
        speciesList.add(new OptionValue("aspergillus_terreus", "aspergillus_terreus"));
        speciesList.add(new OptionValue("bombus_impatiens1", "bombus_impatiens1"));
        speciesList.add(new OptionValue("bombus_terrestris2", "bombus_terrestris2"));
        speciesList.add(new OptionValue("botrytis_cinerea", "botrytis_cinerea"));
        speciesList.add(new OptionValue("brugia", "brugia"));
        speciesList.add(new OptionValue("cacao", "cacao"));
        speciesList.add(new OptionValue("caenorhabditis", "caenorhabditis"));
        speciesList.add(new OptionValue("candida_albicans", "candida_albicans"));
        speciesList.add(new OptionValue("candida_guilliermondii", "candida_guilliermondii"));
        speciesList.add(new OptionValue("candida_tropicalis", "candida_tropicalis"));
        speciesList.add(new OptionValue("chaetomium_globosum", "chaetomium_globosum"));
        speciesList.add(new OptionValue("chicken", "chicken"));
        speciesList.add(new OptionValue("chlamy2011", "chlamy2011"));
        speciesList.add(new OptionValue("chlamydomonas", "chlamydomonas"));
        speciesList.add(new OptionValue("chlorella", "chlorella"));
        speciesList.add(new OptionValue("coccidioides_immitis", "coccidioides_immitis"));
        speciesList.add(new OptionValue("Conidiobolus_coronatus", "Conidiobolus_coronatus"));
        speciesList.add(new OptionValue("coprinus", "coprinus"));
        speciesList.add(new OptionValue("coprinus_cinereus", "coprinus_cinereus"));
        speciesList.add(new OptionValue("cryptococcus", "cryptococcus"));
        speciesList.add(new OptionValue("cryptococcus_neoformans_gattii", "cryptococcus_neoformans_gattii"));
        speciesList.add(new OptionValue("cryptococcus_neoformans_neoformans_B", "cryptococcus_neoformans_neoformans_B"));
        speciesList.add(new OptionValue("cryptococcus_neoformans_neoformans_JEC21", "cryptococcus_neoformans_neoformans_JEC21"));
        speciesList.add(new OptionValue("culex", "culex"));
        speciesList.add(new OptionValue("debaryomyces_hansenii", "debaryomyces_hansenii"));
        speciesList.add(new OptionValue("elegans", "elegans"));
        speciesList.add(new OptionValue("elephant_shark", "elephant_shark"));
        speciesList.add(new OptionValue("encephalitozoon_cuniculi_GB", "encephalitozoon_cuniculi_GB"));
        speciesList.add(new OptionValue("eremothecium_gossypii", "eremothecium_gossypii"));
        speciesList.add(new OptionValue("fly", "fly"));
        speciesList.add(new OptionValue("fusarium", "fusarium"));
        speciesList.add(new OptionValue("fusarium_graminearum", "fusarium_graminearum"));
        speciesList.add(new OptionValue("galdieria", "galdieria"));
        speciesList.add(new OptionValue("generic", "generic"));
        speciesList.add(new OptionValue("heliconius_melpomene1", "heliconius_melpomene1"));
        speciesList.add(new OptionValue("histoplasma", "histoplasma"));
        speciesList.add(new OptionValue("histoplasma_capsulatum", "histoplasma_capsulatum"));
        speciesList.add(new OptionValue("honeybee1", "honeybee1"));
        speciesList.add(new OptionValue("human", "human"));
        speciesList.add(new OptionValue("kluyveromyces_lactis", "kluyveromyces_lactis"));
        speciesList.add(new OptionValue("laccaria_bicolor", "laccaria_bicolor"));
        speciesList.add(new OptionValue("lamprey", "lamprey"));
        speciesList.add(new OptionValue("leishmania_tarentolae", "leishmania_tarentolae"));
        speciesList.add(new OptionValue("lodderomyces_elongisporus", "lodderomyces_elongisporus"));
        speciesList.add(new OptionValue("magnaporthe_grisea", "magnaporthe_grisea"));
        speciesList.add(new OptionValue("maize", "maize"));
        speciesList.add(new OptionValue("maize5", "maize5"));
        speciesList.add(new OptionValue("nasonia", "nasonia"));
        speciesList.add(new OptionValue("neurospora", "neurospora"));
        speciesList.add(new OptionValue("neurospora_crassa", "neurospora_crassa"));
        speciesList.add(new OptionValue("newest", "newest"));
        speciesList.add(new OptionValue("pavar", "pavar"));
        speciesList.add(new OptionValue("pchrysosporium", "pchrysosporium"));
        speciesList.add(new OptionValue("pea_aphid", "pea_aphid"));
        speciesList.add(new OptionValue("pfalciparum", "pfalciparum"));
        speciesList.add(new OptionValue("phanerochaete_chrysosporium", "phanerochaete_chrysosporium"));
        speciesList.add(new OptionValue("pichia_stipitis", "pichia_stipitis"));
        speciesList.add(new OptionValue("pneumocystis", "pneumocystis"));
        speciesList.add(new OptionValue("rhizopus_oryzae", "rhizopus_oryzae"));
        speciesList.add(new OptionValue("rhodnius", "rhodnius"));
        speciesList.add(new OptionValue("saccharomyces", "saccharomyces"));
        speciesList.add(new OptionValue("saccharomyces_cerevisiae_rm11-1a_1", "saccharomyces_cerevisiae_rm11-1a_1"));
        speciesList.add(new OptionValue("saccharomyces_cerevisiae_S288C", "saccharomyces_cerevisiae_S288C"));
        speciesList.add(new OptionValue("schistosoma", "schistosoma"));
        speciesList.add(new OptionValue("schizosaccharomyces_pombe", "schizosaccharomyces_pombe"));
        speciesList.add(new OptionValue("tetrahymena", "tetrahymena"));
        speciesList.add(new OptionValue("tomato", "tomato"));
        speciesList.add(new OptionValue("toxoplasma", "toxoplasma"));
        speciesList.add(new OptionValue("tribolium", "tribolium"));
        speciesList.add(new OptionValue("tribolium2012", "tribolium2012"));
        speciesList.add(new OptionValue("trichinella", "trichinella"));
        speciesList.add(new OptionValue("ustilago", "ustilago"));
        speciesList.add(new OptionValue("ustilago_maydis", "ustilago_maydis"));
        speciesList.add(new OptionValue("verticillium_albo_atrum1", "verticillium_albo_atrum1"));
        speciesList.add(new OptionValue("verticillium_longisporum1", "verticillium_longisporum1"));
        speciesList.add(new OptionValue("yarrowia_lipolytica", "yarrowia_lipolytica"));

        speciesOption = addComboBoxOption("species", "Species", speciesList, defaultSpecies);
    }

    String[] getCommand(String tempFile) {
        ArrayList<String> commandList = new ArrayList<String>();

        String path = AugustusAnnotationGenerator.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = null;
        try {
            decodedPath = URLDecoder.decode(path, "UTF-8");
            //System.out.println("path " + decodedPath);

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        File jarPath = new File(path);
        String newPath = jarPath.getParent();
        String os = System.getProperty("os.name");
        System.out.println("the os is: " + os);
        if (os.contains("Mac OS X")) {
            commandList.add(newPath + "/augustus/augustus.mac");
            commandList.add("--AUGUSTUS_CONFIG_PATH=" + newPath + "/augustus/config");

        } else if (os.contains("Linux")) {
            commandList.add(newPath + "/augustus/augustus.linux");
            commandList.add("--AUGUSTUS_CONFIG_PATH=" + newPath + "/augustus/config");

        } else {
            //must be windows
            commandList.add(newPath+"/augustus/augustus.win");
            commandList.add("--AUGUSTUS_CONFIG_PATH=" + "augustus/config");

        }


        OptionValue selectedSpecies = (OptionValue) speciesOption.getValue();

        commandList.add("--species=" + selectedSpecies.getName());

        commandList.add(tempFile);
        return commandList.toArray(new String[commandList.size()]);

    }
    
    String getWorkingDir() {
        String workingDir = null;
        String path = AugustusAnnotationGenerator.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = null;
        try {
            decodedPath = URLDecoder.decode(path, "UTF-8");
            //System.out.println("path " + decodedPath);

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        File jarPath = new File(path);
        workingDir = jarPath.getParent();
        return workingDir;
    }
}
